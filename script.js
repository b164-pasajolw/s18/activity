console.log("Hello world")


//no.3
let trainer3 = {}

//no.4
trainer3.name = 'Ash Ketchum';
console.log(trainer3.name);

trainer3.age = 10;
console.log(trainer3.age);

trainer3.pokemon = ['Pikachu', 'Catepie', 'Pigeoto', 'Bulbasaur'];
console.log(trainer3.pokemon)

trainer3.friends = {
	TabiTabi: ['Bobby', 'Dina'],
	Hoenn: ['May', 'Max'],
	Kanto: ['Brock', 'Misty']
}
console.log(trainer3.friends);

trainer3.talkAgain = function() {
		console.log("Pikachu! I choose you!")
	}


//no.5
trainer3.talkAgain = function() {
		console.log("Pikachu! I choose you!")
	}

//no.6

console.log('Result of dot notation:');
console.log(trainer3.name);

console.log('Result of dot square bracket notation:');
console.log(trainer3['pokemon']);

//no.7
console.log('Result of talk method:');
trainer3.talkAgain();

console.log('Result of Objects within trainer3:');
console.log(trainer3);


//no.8
function Pokemon(name, level, health, attack) {
	//properties
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = attack;

	//methods
	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`);
		console.log(target.name + "'s" + " health is now reduced to " + (target.health - this.attack) + ".")
	};
	this.faint = function() {
		console.log(`${this.name} fainted.`)
	}
}

//no.9
let squirtle = new Pokemon("Squirtle", 16, 60, 60);
let bulbasuar = new Pokemon("Bulbasaur", 12, 60, 10  );
let charmander = new Pokemon("Charmander", 14, 60, 20);
console.log(squirtle);
console.log(bulbasuar);
console.log(charmander);

//no.10
bulbasuar.tackle(charmander);
charmander.tackle(squirtle);
squirtle.tackle(bulbasuar);

//no.11
bulbasuar.faint();

